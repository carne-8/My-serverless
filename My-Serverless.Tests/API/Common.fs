module Tests.Common

open Expecto
open Microsoft.AspNetCore.TestHost
open FsToolkit.ErrorHandling
open System.Net.Http

type RequestBuilder with
    member this.DeleteAsync () =
        this.SendAsync("DELETE")

    member this.SetJson<'a> (obj: 'a) =
        let str = obj |> Json.toString
        this.And(fun msg ->
            msg.Content <- new StringContent(str, System.Text.Encoding.UTF8, "application/json")
        )

    member this.AddBearerToken token =
        this.AddHeader("Authorization", sprintf "Bearer %s" token )

module RequestBuilder =
    let setJson<'a> (obj: 'a) (rb: RequestBuilder) =
        rb.SetJson(obj)

    let addBearerToken token  (rb: RequestBuilder) =
        rb.AddBearerToken(token)

    let sendRequest method expectedCode (req: RequestBuilder) =
        taskResult {
            let! (response: HttpResponseMessage) = req.SendAsync method
            if response.StatusCode <> expectedCode then
                let content = response.Content.ReadAsStringAsync().Result
                Expect.equal response.StatusCode expectedCode (sprintf "Status code not expected\nBody:\n%s" content)

            return response
        } |> TaskResult.returnOrFail


type HttpResponseMessage with
    member this.ContentFromJson<'a> () =
        task {
            let! stringContent = this.Content.ReadAsStringAsync()
            return stringContent
            |> Json.fromString<'a>
            |> Result.mapError (fun err -> err + "\n" + stringContent)
            |> Result.returnOrFail
        }