module Tests.API

open System
open Expecto
open System.Net
open System.Net.Http
open Tests.Common
open Microsoft.AspNetCore.TestHost

let testRedirects items (testServer: TestServer) =
    task {
        for (path, redirect) in items do
            let request = testServer.CreateRequest path
            let! response = request.GetAsync()

            Expect.equal response.StatusCode HttpStatusCode.Moved "Response status code should be redirect"
            Expect.isNotNull response.Headers.Location "Redirect location header is not present"
            Expect.equal (response.Headers.Location.AbsoluteUri |> String.toLowerInvariant) redirect (sprintf "Redirect location is not working for '%s' got '%s'" path response.Headers.Location.AbsoluteUri)
    }

[<Tests>]
let tests =
    testList "API" [
        testTask "Default route works" {
            use testServer = TestServer.createDefaultServer()

            let req = testServer.CreateRequest "/"

            let! (rawResponse: HttpResponseMessage) =
                req
                |> RequestBuilder.sendRequest "GET" HttpStatusCode.OK

            let! (response: string) = rawResponse.Content.ReadAsStringAsync()

            Expect.equal (response) "Hello, it's carne's API !" "Should say hello"
        }

        testTask "Shortener work" {
            use testServer = TestServer.createDefaultServer()

            do! testServer |> testRedirects [
                "/short/cjaac", "https://cjaac.netlify.app/"
                "/short/test?test=true", "https://cjaac.netlify.app/?test=true"
            ]
        }
    ]