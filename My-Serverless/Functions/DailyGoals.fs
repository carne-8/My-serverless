module MyServerless.DailyGoals

open MyServerless
open System
open System.Threading.Tasks
open FsToolkit.ErrorHandling

open Notion.Client
open Microsoft.Azure.WebJobs
open Microsoft.Extensions.Logging

let notionSecret = Config.getString "notion:secret"
let notionPageId = Config.getString "notion:dailyGoals:pageId"
let notionDatePropertyId = Config.getString "notion:dailyGoals:datePropertyId"

let getPageIcon (now: DateTime) =
    let iconUrl =
        match now.DayOfWeek with
        | DayOfWeek.Monday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/pi%C3%B1ata.png"
        | DayOfWeek.Tuesday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/bullseye.png"
        | DayOfWeek.Wednesday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/sparkles.png"
        | DayOfWeek.Thursday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/game/boxing-glove.png"
        | DayOfWeek.Friday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/yarn.png"
        | DayOfWeek.Saturday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/joystick.png"
        | DayOfWeek.Sunday -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/video-game.png"
        | _ -> "https://raw.githubusercontent.com/JustJordanT/Windows_11_Emjois/main/firecracker.png"

    ExternalFile(External = ExternalFile.Info(Url = iconUrl))

let getDateProperty date =
    DatePropertyValue(Date = Date(Start = Nullable date))

[<FunctionName "GenerateDailyGoalsPages">] // Run every first day of the month at 1:00 AM
let generateDailyGoalsPages ([<TimerTrigger "0 0 1 1 * *">] timer: TimerInfo, logger: ILogger) =
    let notion =
        ClientOptions(AuthToken = notionSecret)
        |> NotionClientFactory.Create

    let now = DateTime.Now.AddMonths 1
    let dayInTheMonth = DateTime.DaysInMonth(now.Year, now.Month)

    List.init dayInTheMonth ((+) 1 >> Helpers.DateTime.FromDayOfTheMonth now)
    // Create page
    |> List.map (fun pageDate -> Helpers.Notion.createPage notionPageId (pageDate.ToLongDateString()), pageDate)
    // Add date property
    |> List.map (fun (page, pageDate) -> page.AddProperty(notionDatePropertyId, (getDateProperty pageDate)), pageDate)
    // Add icon
    |> List.map (fun (page, pageDate) -> pageDate |> getPageIcon |> page.SetIcon)
    // Add todo in page
    |> List.map (fun page ->
        ToDoBlock(ToDo = ToDoBlock.Info(RichText = [ RichTextText(Text = Text(Content = "")) ]))
        |> page.AddPageContent
    )
    // Create pages
    |> List.map (fun page -> page.Build())
    |> List.map (notion.Pages.CreateAsync)
    |> Task.WhenAll
    :> Task


[<FunctionName "CopyYesterdayUndoneToDoInTodayPage">] // Run day at 1:00 AM
let copyYesterdayUndoneToDoInTodayPage ([<TimerTrigger "0 0 1 * * *">] timer: TimerInfo, logger: ILogger) =
    taskResult {
        let notion =
            ClientOptions(AuthToken = notionSecret) |> NotionClientFactory.Create

        let! dayPages =
            notion.Databases.QueryAsync(notionPageId, DatabasesQueryParameters())
            |> Task.map (fun database -> database.Results |> List.ofSeq)

        let! yesterday =
            dayPages
            |> List.tryFind (fun page ->
                let found, value = page.Properties.TryGetValue("Date")

                if found
                then
                    let value = value :?> DatePropertyValue
                    value.Date.Start
                    |> Option.ofNullable
                    |> Option.map (fun (date: DateTime) -> date.Date = DateTime.Now.AddDays(-1.).Date)
                    |> Option.defaultValue false
                else false
            )
            |> Result.requireSome "Yesterday's page not found"

        let! today =
            dayPages
            |> List.tryFind (fun page ->
                let found, value = page.Properties.TryGetValue("Date")

                if found
                then
                    let value = value :?> DatePropertyValue
                    value.Date.Start
                    |> Option.ofNullable
                    |> Option.map (fun (date: DateTime) -> date.Date = DateTime.Now.Date)
                    |> Option.defaultValue false
                else false
            )
            |> Result.requireSome "Today's page not found"

        let! yesterdayUndoneToDo =
            notion.Blocks.RetrieveChildrenAsync(yesterday.Id, BlocksRetrieveChildrenParameters())
            |> Task.map (fun blocks -> blocks.Results |> List.ofSeq)
            |> Task.map (List.filter (fun block -> block.Type = BlockType.ToDo))
            |> Task.map (List.filter (fun (block: IBlock) -> (block :?> ToDoBlock).ToDo.IsChecked |> not))

        match yesterdayUndoneToDo |> List.isEmpty with
        | true -> return ()
        | false ->
            let! _ =
                notion.Blocks.AppendChildrenAsync(
                    today.Id,
                    BlocksAppendChildrenParameters(Children = yesterdayUndoneToDo)
                )

            return ()
    }
    |> TaskOption.ofResultError
    |> TaskOption.map logger.LogError
    |> Task.asTask