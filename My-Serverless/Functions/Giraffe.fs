module Run

open Giraffe
open MyServerless
open System.Threading.Tasks
open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Logging

let app : HttpHandler =
    choose [
        OPTIONS >=> Successful.NO_CONTENT // Cors handling

        routeCi "/" >=> text "Hello, it's carne's API !"
        routeCi "" >=> text "Hello, it's carne's API !"

        MyServerless.API.HttpHandlers.shortener

        [| |] |> setBody |> RequestErrors.notFound // Reply 404 for other routes
    ]

let errorHandler (ex : exn) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> ServerErrors.INTERNAL_ERROR ex.Message


[<FunctionName "Giraffe">]
let run ([<HttpTrigger (AuthorizationLevel.Anonymous, Route = "{*any}")>] req: HttpRequest, context: ExecutionContext, log: ILogger) =
    let func = Some >> Task.FromResult

    { new Microsoft.AspNetCore.Mvc.IActionResult with
        member _.ExecuteResultAsync(ctx) =
          task {
            try
                return! app func ctx.HttpContext :> Task
            with exn ->
                return! errorHandler exn log func ctx.HttpContext :> Task
          }
          :> Task }
