[<AutoOpen>]
module MyServerless.Common

open System
open Microsoft.Extensions.Configuration

module Config =
    let basePath =
            let isDevelopmentEnvironment =
                "Development"
                    .Equals(Environment.GetEnvironmentVariable("AZURE_FUNCTIONS_ENVIRONMENT"),
                            StringComparison.OrdinalIgnoreCase)

            if isDevelopmentEnvironment then
                Environment.GetEnvironmentVariable("AzureWebJobsScriptRoot")
            else
                Environment.GetEnvironmentVariable("HOME")
                + "\\site\\wwwroot"

    let configuration: IConfigurationRoot =

        let conf =
            try
                ConfigurationBuilder().SetBasePath(basePath)
            with
            | _ -> ConfigurationBuilder() :> IConfigurationBuilder

        conf
            .AddJsonFile("appsettings.json", optional = true, reloadOnChange = false) // common settings go here.
            .AddJsonFile("appsettings.Development.json", optional = true, reloadOnChange = false) // environment specific settings go here
            .AddJsonFile("local.settings.json", optional = true, reloadOnChange = false) // secrets go here. This file is excluded from source control.
            .AddEnvironmentVariables()
            .Build()


    let getString path =
        configuration.[path]

    let getAll (startsWith: string) =
        configuration.AsEnumerable()
        |> Seq.choose
            (fun kv ->
                match kv.Key.StartsWith(startsWith) with
                | true -> Some(kv.Key, kv.Value)
                | false -> None)
        |> Seq.toList

module Helpers =
    open System.Collections.Generic

    let list item =
        let list = List()
        list.Add item
        list

    module DateTime =
        let FromDayOfTheMonth (now: DateTime) dayOfTheMonth =
            DateTime(now.Year, now.Month, dayOfTheMonth)

    module Notion =
        open Notion.Client

        let createPage parentDatabaseId title =
            let parent = DatabaseParentInput(DatabaseId = parentDatabaseId)
            let title = RichTextText(Text = Text(Content = title)) :> RichTextBase |> list

            PagesCreateParametersBuilder
                .Create(parent)
                .AddProperty("title", TitlePropertyValue(Title = title))