namespace Startup

open Giraffe
open Microsoft.Azure.Functions.Extensions.DependencyInjection

type Startup() =
    inherit FunctionsStartup()

    override __.Configure(builder: IFunctionsHostBuilder) =
        builder.Services.AddGiraffe() |> ignore

[<assembly: FunctionsStartup(typeof<Startup>)>]
do ()