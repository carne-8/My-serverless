#r "paket:
nuget Fake.DotNet.Cli
nuget Fake.IO.FileSystem
nuget Fake.IO.Zip
nuget Fake.Core.Target
nuget BlackFox.CommandLine //"
#load ".fake/build.fsx/intellisense.fsx"

open Fake.Core
open Fake.Core.TargetOperators
open Fake.IO
open Fake.IO.Globbing.Operators
open Fake.DotNet
open BlackFox.CommandLine


let shellExec cmd =
    if Environment.isWindows then CreateProcess.fromRawCommand "cmd" [ "/C"; cmd ]
    else CreateProcess.fromRawCommand "bash" [ "-c"; cmd ]
    |> Proc.run

let shellExecWithOutput cmd =
    if Environment.isWindows then CreateProcess.fromRawCommand "cmd" [ "/C"; cmd ]
    else CreateProcess.fromRawCommand "bash" [ "-c"; cmd ]
    |> CreateProcess.redirectOutput
    |> Proc.run

let shellExecOrFail error cmd =
    let result = shellExec cmd
    if result.ExitCode <> 0 then failwith error


Target.create "AzureLogin" (fun _ ->
    let args =
        CmdLine.empty
        |> CmdLine.append "az"
        |> CmdLine.append "login"
        |> CmdLine.append "--service-principal"
        |> CmdLine.appendPrefix "--username" (Environment.environVarOrFail "AZURE_SERVICE_PRINCIPAL")
        |> CmdLine.appendPrefix "--password" (Environment.environVarOrFail "AZURE_SERVICE_PRINCIPAL_PWD")
        |> CmdLine.appendPrefix "--tenant" (Environment.environVarOrFail "AZURE_TENANT")
        |> CmdLine.toString

    shellExecOrFail "Azure login failed" args
)

Target.create "LoadEnvironmentVariables" (fun _ ->
    !! "My-Serverless/appsettings.json"
    |> Shell.replaceInFiles [
        "NOTION_SECRET", Environment.environVarOrFail "NOTION_SECRET"
        "NOTION_SHORTENER_DATABASE_ID", Environment.environVarOrFail "NOTION_SHORTENER_DATABASE_ID"
        "NOTION_SHORTENER_ROUTE_PROPERTY_ID", Environment.environVarOrFail "NOTION_SHORTENER_ROUTE_PROPERTY_ID"
        "NOTION_SHORTENER_REDIRECT_ROUTE_PROPERTY_ID", Environment.environVarOrFail "NOTION_SHORTENER_REDIRECT_ROUTE_PROPERTY_ID"
        "NOTION_DAILY_GOAL_PAGE_ID", Environment.environVarOrFail "NOTION_DAILY_GOAL_PAGE_ID"
    ]
)

Target.create "Build" (fun _ ->
    DotNet.publish
        (fun opt -> { opt with OutputPath = Some "publish" } )
        "My-Serverless/My-Serverless.fsproj"

    !! "publish/local.settings.json"
    |> Seq.iter Shell.rm
)

Target.create "Deploy" (fun _ ->
    !! "publish/**/*"
    |> Zip.filesAsSpecs "publish"
    |> Zip.zipSpec "my-serverless.zip"

    let zipFullPath = !! "my-serverless.zip" |> Seq.head

    "az functionapp deployment source config-zip -g gael -n carne8 --src " + zipFullPath
    |> shellExecOrFail "Deployment failed"
)

Target.create "Tests" (fun _ ->
    "dotnet run --project My-Serverless.Tests -- --junit-summary TestResults.junit.xml --fail-on-focused-tests --no-spinner"
    |> shellExecOrFail "Unit tests error"
)

"LoadEnvironmentVariables" ==> "Build"

"AzureLogin" ==> "Deploy"

"LoadEnvironmentVariables" ==> "Tests"

Target.runOrDefault "Build"